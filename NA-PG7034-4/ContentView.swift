//
//  ContentView.swift
//  NA-PG7034-4
//
//  Created by Patrik Gregovic (RIT Student) on 11/17/21.
//  Copyright © 2021 Patrik Gregovic (RIT Student). All rights reserved.
//

import SwiftUI

struct ContentView: View {
    
    @ObservedObject private var articleViewModel = ArticlesViewModel()
    @ObservedObject private var newsStandViewModel = NewsStandsViewModel()

    
    var body: some View {
            TabView {
                NavigationView {
                    NewsListView(articleViewModel: articleViewModel)
                        .navigationBarTitle("News")
                        .navigationBarItems(
                            trailing:
                                Button(
                                    action: {
                                        self.articleViewModel.toggleIsCompact()
                                    },
                                    label: {
                                        if(self.articleViewModel.isCompact) {
                                            Text("Non-Compact")
                                        } else {
                                            Text("Compact")
                                        }
                                    }
                                )
                        )
                }.tabItem({
                    Image(systemName: "waveform.path.ecg")
                    Text("News")
                })
                NavigationView {
                    BookmarksListView(articleViewModel: articleViewModel)
                        .navigationBarTitle("Bookmarks")
                }.tabItem({
                    Image(systemName: "bookmark.fill")
                    Text("Bookmarks")
                })
                NavigationView {
                    //NewsStandListView(newsStandViewModel: newsStandViewModel)
                    NewsStands(newsStandViewModel: newsStandViewModel).navigationBarTitle("News Stands")
                }.tabItem({
                    Image(systemName: "location")
                    Text("News Stands")
                })
        }
    }
}
