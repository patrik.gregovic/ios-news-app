//
//  ArticlesViewModel.swift
//
//  Created by Andrej Saric.
//

import Foundation
import Combine

final class ArticlesViewModel: ObservableObject {
    @Published private(set) var data: [Article] = []
    @Published private(set) var bookmarks: [Article] = []
    @Published private(set) var isCompact = false
    
    private var cancellable: Set<AnyCancellable> = []

    init() {
        getData()
    }

    func getData() {
        guard let url = URL(string: "https://70o99.mocklab.io/news") else {
            fatalError("Url cannot be created.")
        }

        let jsonDecoder = JSONDecoder()
        jsonDecoder.dateDecodingStrategy = .iso8601

        URLSession.shared
            .dataTaskPublisher(for: url)
            .tryMap({ $0.data })
            .decode(type: [Article].self, decoder: jsonDecoder)
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { print($0) },
                  receiveValue: {
                    self.data = $0
                  })
            .store(in: &cancellable)
    }

    func toggleBookmark(article: Article) {
        if let index = bookmarks.firstIndex(where: { $0.id == article.id }) {
            bookmarks.remove(at: index)
            return
        }

        bookmarks.append(article)
    }

    func deleteBookmark(offset: IndexSet) {
        bookmarks.remove(atOffsets: offset)
    }
    
    public func toggleIsCompact (){
        self.isCompact.toggle()
    }
    
    public func isBookmarked (article: Article) -> Bool {
        return self.bookmarks.contains(article)
    }
    
    public func getRelatedArticles ( article: Article ) -> [Article] {
        return self.data.filter{ article.relatedArticles.contains( Int($0.id) ) }
    }
    
}
