//
//  NewsStandsViewModel.swift
//
//  Created by Andrej Saric.
//

import Foundation
import Combine

final class NewsStandsViewModel: ObservableObject {
    @Published var data: [NewsStand] = []

    private var cancellable: Set<AnyCancellable> = []

    init() {
        getData()
    }

    func getData() {
        guard let url = URL(string: "https://70o99.mocklab.io/newsstand") else {
            fatalError("Url cannot be created.")
        }

        let jsonDecoder = JSONDecoder()
        jsonDecoder.dateDecodingStrategy = .iso8601

        URLSession.shared
            .dataTaskPublisher(for: url)
            .tryMap({ $0.data })
            .decode(type: [NewsStand].self, decoder: jsonDecoder)
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { print($0) },
                  receiveValue: { self.data = $0 })
            .store(in: &cancellable)
    }
}

