//
//  ImageExtensions.swift
//  NewsApp
//
//  Created by Andrej Saric.
//

import SwiftUI
import SDWebImageSwiftUI

extension Image {
    typealias PlaceholderBuilder<Placeholder> = () -> Placeholder
    
    static func loadFrom<Content: View>(url: URL?,
                                        @ViewBuilder placeholder: PlaceholderBuilder<Content>) -> some View {
        WebImage(url: url)
            .resizable()
            .placeholder {
                placeholder()
            }
            .indicator(.activity)
            .aspectRatio(contentMode: .fill)
    }
}

