//
//  ArrayExtensions.swift
//  NA-PG7034-4
//
//  Created by Patrik Gregovic (RIT Student) on 11/24/21.
//  Copyright © 2021 Patrik Gregovic (RIT Student). All rights reserved.
//

import Foundation

extension Array where Element: Equatable {
    func all(where predicate: (Element) -> Bool) -> [Element]  {
        return self.compactMap { predicate($0) ? $0 : nil }
    }
}
