//
//  NewsStandListView.swift
//  na-pg7034-4
//
//  Created by Patrik Gregovic (RIT Student) on 10/18/21.
//  Copyright © 2021 Patrik Gregovic (RIT Student). All rights reserved.
//

import SwiftUI

struct NewsStandListView: View {
    
    @ObservedObject var  newsStandViewModel: NewsStandsViewModel
     
    private var data: [NewsStand] {
        return newsStandViewModel.data
    }
        
        var body: some View {
            Group {
                if data.isEmpty {
                    Text("No data to show")
                } else {
                    List {
                        ForEach(data) { newsStand in
                            NewsStandRowView(newsStand: newsStand)
                        }
                    }
                }
            }.onAppear {
                UITableView.appearance().separatorStyle = .singleLine
            }
        }
    }
