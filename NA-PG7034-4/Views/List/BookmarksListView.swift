//
//  BookmarksListView.swift
//  NA-PG7034-4
//
//  Created by Patrik Gregovic (RIT Student) on 11/17/21.
//  Copyright © 2021 Patrik Gregovic (RIT Student). All rights reserved.
//

import SwiftUI

struct BookmarksListView: View {
    
    @ObservedObject var  articleViewModel: ArticlesViewModel
    
    private var bookmarks: [Article] {
        return articleViewModel.bookmarks
    }
    
    private var isCompact: Bool {
        return articleViewModel.isCompact 
    }
        
    @ViewBuilder
    var body: some View {
        Group {
            if bookmarks.isEmpty {
                Text("")
            } else {
               List {
                    ForEach(bookmarks) { article in
                        if(self.isCompact){
                            NavigationLink(
                            destination:
                                ArticleDetailView(
                                    relatedArticles: self.articleViewModel.getRelatedArticles(article: article),
                                    article: article
                                )
                                .navigationBarTitle(article.source)
                                .navigationBarItems(
                                    trailing:
                                        Button(
                                            action: {self.articleViewModel.toggleBookmark(article: article)},
                                            label:{
                                            if(
                                                self.articleViewModel.isBookmarked(article: article)){
                                                    Image(systemName: "bookmark.fill")
                                                }else{
                                                    Image(systemName: "bookmark")
                                                }
                                            }
                                        )
                                )
                            ){NewsRowView(article: article, isCompact: self.isCompact, width: UIScreen.main.bounds.width-40, height: 250)}
                        }else{
                            NewsRowView(article: article, isCompact: self.isCompact, width: UIScreen.main.bounds.width-40, height: 250)
                            .overlay(NavigationLink(
                                destination:
                                    ArticleDetailView(
                                        relatedArticles: self.articleViewModel.getRelatedArticles(article: article),
                                        article: article
                                    )
                                    .navigationBarTitle(article.source)
                                    .navigationBarItems(
                                        trailing:
                                            Button(
                                                action: {self.articleViewModel.toggleBookmark(article: article)},
                                                label:{
                                                if(
                                                    self.articleViewModel.isBookmarked(article: article)){
                                                        Image(systemName: "bookmark.fill")
                                                    }else{
                                                        Image(systemName: "bookmark")
                                                    }
                                                }
                                            )
                                    ),
                                label: {EmptyView()}
                            ))
                        }
                    }.onDelete(perform: articleViewModel.deleteBookmark)
                }
            }
        }.onAppear {
            UITableView.appearance().separatorStyle = .none
        }
    }
}
