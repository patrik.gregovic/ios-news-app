//
//  NewsRowView.swift
//  na-pg7034-4
//
//  Created by Patrik Gregovic (RIT Student) on 10/18/21.
//  Copyright © 2021 Patrik Gregovic (RIT Student). All rights reserved.
//

import SwiftUI

struct NewsRowView: View {
    var article: Article
    var isCompact: Bool
    var width: CGFloat
    var height: CGFloat
        
    @ViewBuilder
    var body: some View {
        if (isCompact){
            VStack(alignment: .center) {
                HStack(alignment: .center){
                    VStack(alignment: .leading, spacing: 8) {
                        Text(article.title)
                            .font(.body)
                            .fontWeight(.semibold)
                            .lineLimit(1)
                            .fixedSize(horizontal: false, vertical: true)
                        Text(article.content)
                            .font(.caption)
                            .fontWeight(.regular)
                            .foregroundColor(.secondary)
                            .lineLimit(2)
                            .fixedSize(horizontal: false, vertical: true)
                    }
                    
                    Image.loadFrom(url: article.imageUrl) {
                        Rectangle()
                    }
                    .frame(width: 90, height: 90)
                    .cornerRadius(12)
                }
            }
        } else {
                VStack(alignment: .leading, spacing: 8) {
                    Image.loadFrom(url: article.imageUrl) {
                        Rectangle()
                    }
                    .frame(width: self.width, height: self.height)
                    .cornerRadius(12)
                    Text(article.title)
                        .font(.body)
                        .fontWeight(.medium)
                        .fixedSize(horizontal: false, vertical: true)
                }.frame(width: self.width)
        }
    }
}
