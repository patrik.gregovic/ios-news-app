//
//  NewsStandRowView.swift
//  na-pg7034-4
//
//  Created by Patrik Gregovic (RIT Student) on 10/18/21.
//  Copyright © 2021 Patrik Gregovic (RIT Student). All rights reserved.
//

import SwiftUI

struct NewsStandRowView: View {
    var newsStand: NewsStand
        
    var body: some View {
        VStack(alignment: .leading, spacing: 8) {
            Text(newsStand.name)
                .font(.body)
                .fontWeight(.semibold)
                .lineLimit(1)
                .fixedSize(horizontal: false, vertical: true)
            Text(newsStand.category.description)
                .font(.body)
                .lineLimit(1)
                .fixedSize(horizontal: false, vertical: true)
        }
    }
}
