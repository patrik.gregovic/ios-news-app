//
//  NewsListView.swift
//  na-pg7034-4
//
//  Created by Patrik Gregovic (RIT Student) on 10/18/21.
//  Copyright © 2021 Patrik Gregovic (RIT Student). All rights reserved.
//

import SwiftUI

struct NewsListView: View {
    
    @ObservedObject var  articleViewModel: ArticlesViewModel
    
    private var data: [Article] {
        return articleViewModel.data
        
    }
    private var isCompact: Bool {
        return articleViewModel.isCompact
    }
        
    @ViewBuilder
    var body: some View {
        Group {
            if data.isEmpty {
                Text("No data to show")
            } else {
                List {
                    ForEach(data) { article in
                        if(self.isCompact){
                            NavigationLink(
                            destination:
                                ArticleDetailView(
                                    relatedArticles: self.articleViewModel.getRelatedArticles(article: article),
                                    article: article
                                )
                                .navigationBarTitle(article.source)
                                .navigationBarItems(
                                    trailing:
                                        Button(
                                            action: {self.articleViewModel.toggleBookmark(article: article)},
                                            label:{
                                                if(self.articleViewModel.isBookmarked(article: article)){
                                                    Image(systemName: "bookmark.fill")
                                                }else{
                                                    Image(systemName: "bookmark")
                                                }
                                            }
                                        )
                                )
                            ){NewsRowView(article: article, isCompact: self.isCompact, width: UIScreen.main.bounds.width-40, height: 250)
                            }
                        }else{
                            NewsRowView(article: article, isCompact: self.isCompact, width: UIScreen.main.bounds.width-40, height: 250)
                            .overlay(NavigationLink(
                                destination:
                                    ArticleDetailView(
                                        relatedArticles: self.articleViewModel.getRelatedArticles(article: article),
                                        article: article
                                    )
                                    .navigationBarTitle(article.source)
                                    .navigationBarItems(
                                        trailing:
                                            Button(
                                                action: {self.articleViewModel.toggleBookmark(article: article)},
                                                label:{
                                                if(
                                                    self.articleViewModel.isBookmarked(article: article)){
                                                        Image(systemName: "bookmark.fill")
                                                    }else{
                                                        Image(systemName: "bookmark")
                                                    }
                                                }
                                            )
                                    ),
                                label: {EmptyView()}
                            ))
                        }
                    }
                }
            }
        }.onAppear {
            UITableView.appearance().separatorStyle = .none
        }
    }
}
