//
//  NewsStandAnnotation.swift
//  NA-PG7034-4
//
//  Created by Patrik Gregovic (RIT Student) on 11/30/21.
//  Copyright © 2021 Patrik Gregovic (RIT Student). All rights reserved.
//

import MapKit

class NewsStandAnnotation: NSObject, MKAnnotation {
    
    var newsStand: NewsStand
    
    init(newsStand: NewsStand) {
        self.newsStand = newsStand
    }
    
    var coordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: self.newsStand.location.latitude,
        longitude: self.newsStand.location.longitude)
    }
    
    var title: String? {
        return self.newsStand.name
    }
    
    var subtitle: String? {
        return self.newsStand.category.description
    }
}
