//
//  MapView.swift
//  NA-PG7034-4
//
//  Created by Patrik Gregovic (RIT Student) on 11/30/21.
//  Copyright © 2021 Patrik Gregovic (RIT Student). All rights reserved.
//

import SwiftUI
import MapKit

enum DirectionsError: Error, LocalizedError {
    case userLocationNotAvailable
    case url(message: String)

    var errorDescription: String? {
        return localizedDescription
    }

    var localizedDescription: String {
        switch self {
        case .userLocationNotAvailable:
            return "User location is not available. Please enable it in Settings."
        case .url(let message):
            return message
        }
    }
}


struct MapView: UIViewRepresentable {
    
    struct MapState: Identifiable {
        let id = UUID()
        var newsStand: NewsStand? = nil
        var error: Error? = nil
    }
    
    @Binding var data: [NewsStand]
    @Binding var mapState: MapState?
    var mapType: MKMapType
    
    func makeCoordinator() -> MapView.Coordinator {
        return Coordinator(parent: self)
    }
    
    func makeUIView(context: UIViewRepresentableContext<MapView>) -> MKMapView {
        context.coordinator.zoomToRegion(center: CLLocationCoordinate2D(latitude: 45.795399,
                                                                        longitude: 15.976568),
                                         regionRadius: 5000)
        context.coordinator.mapView.mapType = mapType
        return context.coordinator.mapView
    }
    
    func updateUIView(_ uiView: MKMapView, context: UIViewRepresentableContext<MapView>) {
        context.coordinator.requestLocationAuthorization()
        context.coordinator.createAnnotations(data: self.data)
        context.coordinator.mapView.mapType = self.mapType
    }
    
    class Coordinator: NSObject, MKMapViewDelegate {

        var parent: MapView
        var mapView: MKMapView
        private var locationManager: CLLocationManager
        
        init(parent: MapView) {
            self.parent = parent
            self.mapView = MKMapView()
            self.locationManager = CLLocationManager()
            super.init()
            self.mapView.delegate = self
        }
        
        func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
            
            if annotation is MKUserLocation { return nil }
            
            guard let newsStandAnnotation = annotation as? NewsStandAnnotation
                else {
                return nil
            }
            
            if let annotationView = mapView
                .dequeueReusableAnnotationView(withIdentifier: "newsStandAnnotation") as? MKMarkerAnnotationView {
                
                annotationView.annotation = newsStandAnnotation
                annotationView.glyphImage = UIImage(named: newsStandAnnotation.newsStand.category.description.lowercased())
                annotationView.markerTintColor = newsStandAnnotation.newsStand.category.tintColor
                return annotationView
            }
            
            let annotationView = MKMarkerAnnotationView(annotation: newsStandAnnotation,
                reuseIdentifier: "newsStandAnnotation")
            
            annotationView.glyphImage = UIImage(named: newsStandAnnotation.newsStand.category.description.lowercased())
            annotationView.markerTintColor = newsStandAnnotation.newsStand.category.tintColor
            annotationView.canShowCallout = true
            
            let infoButton = UIButton(type: .infoLight)
            
            let carButton = UIButton(type: .infoLight)
            let carImage = UIImage(systemName: "car")
            carButton.setImage(carImage, for: .normal)
            
            infoButton.tag = 0
            carButton.tag = 1
            
            annotationView.leftCalloutAccessoryView = infoButton
            annotationView.rightCalloutAccessoryView = carButton
            
            return annotationView
        }
        
        func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
            
            guard let newsStandAnnotation = view.annotation as? NewsStandAnnotation else {
                return
            }
            
            switch control.tag {
            case 0:
                self.parent.mapState = MapState(newsStand: newsStandAnnotation.newsStand)
            case 1:
                do {
                    try self.openDirections(for: newsStandAnnotation.newsStand)
                } catch {
                    self.parent.mapState = MapState(error: error)
                }
            default:
                return
            }
            
        }

        private func openDirections(for newsStand: NewsStand) throws {
            guard let userLocation = locationManager.location else {
                throw DirectionsError.userLocationNotAvailable
            }

            let userLat = userLocation.coordinate.latitude
            let userLongitude = userLocation.coordinate.longitude

            let destinationLat = newsStand.location.latitude
            let destinationLongitude = newsStand.location.longitude


            let urlString = "https://maps.apple.com/?daddr=(\(destinationLat),\(destinationLongitude))&dirflg=d&saddr=(\(userLat),\(userLongitude))"

            guard let url = URL(string: urlString) else {
                throw DirectionsError.url(message: "Url not valid")
            }

            guard UIApplication.shared.canOpenURL(url) else {
                throw DirectionsError.url(message: "Cannot open url")
            }

            UIApplication.shared.open(url, options: [:])
        }
        
        func requestLocationAuthorization() {
            if(CLLocationManager.authorizationStatus() != .authorizedWhenInUse) {
                locationManager.requestWhenInUseAuthorization()
            }
            
            mapView.showsUserLocation = true
            locationManager.startUpdatingLocation()
        }

        func zoomToRegion(center: CLLocationCoordinate2D, regionRadius: CLLocationDistance)  {

            let region = MKCoordinateRegion(center: center,
                                            latitudinalMeters: regionRadius,
                                            longitudinalMeters: regionRadius)

            mapView.setRegion(region, animated: true)
        }
        
        func createAnnotations(data: [NewsStand]) {
            let existingAnnotations = mapView.annotations.filter({ !($0 is MKUserLocation) })
            mapView.removeAnnotations(existingAnnotations)

            let annotations = data.map { newsStand in NewsStandAnnotation(newsStand: newsStand) }
            mapView.addAnnotations(annotations)
        }
    }
}
