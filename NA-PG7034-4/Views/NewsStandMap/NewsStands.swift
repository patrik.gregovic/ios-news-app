//
//  NewsStands.swift
//  NA-PG7034-4
//
//  Created by Patrik Gregovic (RIT Student) on 11/30/21.
//  Copyright © 2021 Patrik Gregovic (RIT Student). All rights reserved.
//

import SwiftUI
import MapKit

struct NewsStands: View {
    
    @ObservedObject var  newsStandViewModel: NewsStandsViewModel
    @State private var mapState: MapView.MapState? = nil
    @State private var mapType: MKMapType = .standard
    @State private var pickerOptions = [
        PickerOption(title: "Standard", map: .standard),
        PickerOption(title: "Hybrid", map: .hybrid),
        PickerOption(title: "Satellite", map: .satellite)]
     
    private var data: [NewsStand] {
        return newsStandViewModel.data
    }
    
    var body: some View {
        ZStack(alignment: .top){
            VStack {
                MapView(data: $newsStandViewModel.data, mapState: $mapState, mapType: mapType)
            }.alert(item: $mapState) {
                Alert(title: Text($0.newsStand?.name ?? $0.error?.localizedDescription ?? ""), message: Text($0.newsStand?.category.description ?? ""))
            }
            VStack(){
                Spacer().frame(height: 15)
                HStack(alignment: .top) {
                    Picker("", selection: $mapType) {
                        ForEach(pickerOptions, id: \.self) {
                            Text($0.title).tag($0.map)
                        }
                    }.pickerStyle(SegmentedPickerStyle())
                }.frame(width: UIScreen.main.bounds.width-40)
                Spacer()
            }
            
        }
    }
}

struct PickerOption: Hashable {
    var title: String
    var map: MKMapType
}
