//
//  ArticleDetailView.swift
//  NA-PG7034-4
//
//  Created by Patrik Gregovic (RIT Student) on 11/24/21.
//  Copyright © 2021 Patrik Gregovic (RIT Student). All rights reserved.
//

import SwiftUI

struct ArticleDetailView: View {
    var  relatedArticles: [Article]
    var article: Article

    @ViewBuilder
    var body: some View {
        ScrollView(.vertical, showsIndicators: false){
            VStack(){
                Image.loadFrom(url: article.imageUrl) {
                    Rectangle()
                }.frame(width: UIScreen.main.bounds.width,height: 290)
                VStack(alignment: .leading, spacing: CGFloat(20)){
                    HStack(alignment: .center){
                        if ((article.author) != nil){
                            InfoBox(image:"pencil", label: article.author ?? "")
                        }
                        InfoBox(image:"eye", label:String(article.views))
                        InfoBox(image:"star", label:String(article.rating))
                    }
                    Text(article.title).font(.system(size: 30, weight: .regular, design: .default))
                    Text(article.content)
                    Button(
                        action: {UIApplication.shared.open(self.article.url)},
                        label: {Text(article.url.absoluteString).foregroundColor(.blue)}
                    )
                    if( !relatedArticles.isEmpty ){
                        Text("Related").font(.system(size: 30, weight: .regular, design: .default))
                        ScrollView(.horizontal, showsIndicators: false){
                            HStack(alignment: .top, spacing: CGFloat(50)){
                                ForEach(self.relatedArticles) { article in
                                    NewsRowView(article: article, isCompact: false, width: UIScreen.main.bounds.width-100, height: 280)
                                }
                            }.frame(minHeight: 400)
                        }
                    }
                }.padding(10)
            }
        }
    }
    
}
