//
//  InfoBox.swift
//  NA-PG7034-3
//
//  Created by Patrik Gregovic (RIT Student) on 11/24/21.
//  Copyright © 2021 Patrik Gregovic (RIT Student). All rights reserved.
//

import SwiftUI

struct InfoBox: View {

    var image: String//favorite, pencil, eye
    var label: String
    
    var body: some View {
        HStack(alignment: .center, spacing: 7){
            Image(systemName: image).foregroundColor(Color.gray.opacity(1))
            Text(label).foregroundColor(Color.gray.opacity(1)).font(.system(size: 18, weight: .regular, design: .default))
        }.padding(16).background(
            RoundedRectangle(cornerRadius: 8)
                .fill(Color.gray.opacity(0.1))
        )
        
    }
}
